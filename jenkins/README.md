# Jenkins2: Servidor de integración contínua

Para ejecutar los ejercicios de este curso se necesita un servidor Jenkins2.

En este repositorio tienes todo lo necesario para construir un contenedor Docker con todo lo necesario.

## 1 - Construcción de la imagen de Docker para Jenkins2

 - Ejecuta el comando `docker build -t jenkins2 .`

## 2 - Lanza el contenedor de Jenkins2

 - Ejecuta el siguiente comando:

`docker run -p 8080:8080 -p 50000:50000 --name=jenkins-master -d jenkins2`

## Acceso a Jenkins2

Una vez lanzado el contenedor puedes acceder a través de la dirección [http://localhost:8080](http://localhost:8080).

Para acceder puedes usar la cuenta `admin/admin` o cualquier `alumnoX/alumnoX`.

## Parar y relanzar el contenedor de Jenkins2

Podrás parar el contenedor con el comando `docker stop jenkins-master` y lanzarlo de nuevo con el comando `docker start jenkins-master`.

