const path = require('path');

module.exports = {
  entry: './src/string-calculator-cli.js',
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'sc.js'
  }
};
