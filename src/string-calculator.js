import {split, toInt, replaceAll} from './fp/strings';
import {map, filter, reduce, peek} from './fp/arrays';
import {sum, lessThan} from './fp/numbers';
import {pipe} from './fp/fns';

const normalizar = texto => {
  if (texto.startsWith('//')) {
    const separador = texto[2];
    texto = texto.substring(4);
    texto = replaceAll(texto, separador, ',');
  }

  return replaceAll(texto, '\n', ',');
};

const checkNegativos = numeros => {
  const numerosNegativos = numeros.filter(lessThan(0));
  if (numerosNegativos.length > 0)
    throw new Error('Números negativos no admitidos: ' + numerosNegativos.join(', '));
};

export const sc = pipe([
  normalizar,
  split(','),
  map(toInt),
  peek(checkNegativos),
  filter(lessThan(1000)),
  reduce(sum, 0)
]);
