import R from 'ramda';

const sumar = R.curry((a, b) => a + b);

const esPar = n => n % 2 === 0;

const not = predicate => n => !predicate(n);

const cuadrado= n => n * n;

const combine = (f1, f2) => input => f2(f1(input));

const identity = i => i;

const pipe = (fnArray) => fnArray.reduce(combine, identity);

export const procesar = numeros => numeros
      .map(pipe([sumar(10), cuadrado]))
      .filter(not(esPar))
      .reduce(sumar, 0);
