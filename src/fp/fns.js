export const combine = (f1, f2) => input => f2(f1(input));

export const identity = i => i;

export const pipe = (fnArray) => fnArray.reduce(combine, identity);
