export const map = mapper => array => array.map(mapper);

export const filter = predicate => array => array.filter(predicate);

export const reduce = (combineFn, i) => array => array.reduce(combineFn, i);

export const peek = fn => value => {
  fn(value);
  return value;
};