export const split = separator => s => s.split(separator);

export const replaceAll = (text, search, replacement) => {
  return text.replace(new RegExp(search, 'g'), replacement);
};

export const toInt = s => parseInt(s, 10);
