import {replaceAll} from './strings';

describe('replaceAll', () => {
  it('funciona', () => {
    replaceAll('aaa', 'a', 'b').should.eql('bbb');
  });
});